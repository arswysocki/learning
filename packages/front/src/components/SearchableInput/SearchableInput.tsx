import "./SearchableInput.css";
import React from "react";
import { Input, Select, AutoComplete, Card, Spin } from "antd";
import { constants } from "../../constants/constants";
import { graphql } from "react-relay";
import { fetchQuery, useRelayEnvironment } from "react-relay/hooks";
import { SearchableInputQueryResponse } from "./__generated__/SearchableInputQuery.graphql";
import { SelectValue } from "antd/lib/select";
import { Link } from "react-router-dom";
import { SearchByType, TrainingType } from "./types";

const query = graphql`
  query SearchableInputQuery($searchBy: String!, $searchText: String) {
    searchableTrainings(searchBy: $searchBy, searchText: $searchText) {
      trainingId: id
      name
      description
      category {
        categoryId: id
        description
      }
      start
      end
      format {
        description
      }
      audience {
        description
      }
    }
  }
`;

export const SearchableInput: React.FC = () => {
  const enviroment = useRelayEnvironment();
  const searchableInput = React.useRef<Select<SelectValue> | null>(null);
  const [text, setText] = React.useState<string | null>(null);
  const [timer, setTimer] = React.useState<NodeJS.Timeout>();
  const [resultOfSearch, setResultOfSearch] = React.useState<TrainingType[]>(
    []
  );
  const [searchBy, setSearchBy] = React.useState<SearchByType>("name");
  const searchQuery = fetchQuery(enviroment, query, {
    searchBy,
    searchText: text,
  });

  const searchByMap: { [key in SearchByType]: string } = {
    name: "контексту",
    date: "дате",
    format: "формату",
    category: "категории",
    audience: "аудитории",
  };

  const onSearch = (searchText: string) => {
    if (text !== searchText) {
      if (timer) {
        clearTimeout(timer);
      }

      setTimer(
        setTimeout(() => {
          setText(searchText.trim());
        }, 500)
      );
    }
  };

  const onChangeSelect = (value: SelectValue) => {
    setSearchBy(value as SearchByType);
  };

  const NotFoundContent = (
    <>
      {!resultOfSearch.length && text ? (
        <div>Поиск не дал результатов</div>
      ) : (
        <div>Введите текст для поиска</div>
      )}
    </>
  );

  React.useEffect(() => {
    if (text) {
      searchQuery.subscribe({
        next: (data) => {
          setResultOfSearch(
            (data as SearchableInputQueryResponse)
              .searchableTrainings as TrainingType[]
          );
        },
      });
    } else {
      setResultOfSearch([]);
    }
  }, [searchBy, text]);

  return (
    <Input.Group>
      <AutoComplete
        allowClear
        className="searchable-input-form"
        placeholder={constants["SEARCHABLEINPUTPLACEHOLDER"]}
        style={{ width: "50%" }}
        onSearch={onSearch}
        ref={searchableInput}
        notFoundContent={NotFoundContent}
        onSelect={() => {
          searchableInput.current?.blur();
        }}
      >
        {resultOfSearch.length &&
          resultOfSearch.map((training) => (
            <AutoComplete.Option
              key={`${training.name}-${training.trainingId}`}
              value={training.name}
            >
              <Link
                to={`/category/${training.category.categoryId}/training/${training.trainingId}`}
              >
                <Card
                  title={training.name}
                  style={{
                    lineHeight: "0.5rem",
                    fontSize: "small",
                    textOverflow: "ellipsis",
                  }}
                >
                  <p>{training.description}</p>
                  <p>Категория: {training.category.description}</p>
                  <p>Дата: {`${training.start} - ${training.end}`}</p>
                  <p>Аудитория: {training.audience.description}</p>
                  <p>Формат обучения: {training.format.description}</p>
                </Card>
              </Link>
            </AutoComplete.Option>
          ))}
      </AutoComplete>
      <Select
        onChange={onChangeSelect}
        value={`Поиск по: ${searchByMap[searchBy]}`}
      >
        <Select.Option value="name">{searchByMap["name"]}</Select.Option>
        <Select.Option value="date">{searchByMap["date"]}</Select.Option>
        <Select.Option value="format">{searchByMap["format"]}</Select.Option>
        <Select.Option value="category">
          {searchByMap["category"]}
        </Select.Option>
        <Select.Option value="audience">
          {searchByMap["audience"]}
        </Select.Option>
      </Select>
    </Input.Group>
  );
};
