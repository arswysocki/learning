export type TrainingType = {
  trainingId: number;
  name: string;
  description: string;
  category: { categoryId: number; description: string };
  start: string;
  end: string;
  format: {
    description: string;
  };
  audience: {
    description: string;
  };
};

export type SearchByType = "name" | "date" | "format" | "audience" | "category";
